#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include <complex>

const static std::complex<double> I(0.0, 1.0);
const static double pi = 3.14159265358979323846264;


/////////////////////////////////////////////////////////////////////////////////////////////
// Operations on Matsubara functions ////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

// Get Matsubara Frequencies
double wn(const int &n, const double &beta)
{
	return (2.0*n+1.0)*pi/beta;
}

// Calculate the Selfenergy from Dyson's equation
void getSigma(std::complex<double> **Sigma,
              const std::complex<double> *const*G0, const std::complex<double> *const*G, const int &no_wn, const int &no_orb)
{
    for (int n=0; n<no_wn; n++)
        for (int m=0; m<no_orb; m++)
            Sigma[n][m] = (G[n][m]-G0[n][m])/(G0[n][m]*G[n][m]);
}

// Calculate G0 from Dyson's equation
void getG0Dyson(std::complex<double> **G0, const std::complex<double> *const*Sigma,
                const std::complex<double> *const*G, const int &no_wn, const int &no_orb)
{
    for (int n=0; n<no_wn; n++)
        for (int m=0; m<no_orb; m++)
            G0[n][m] = G[n][m]/( 1.0 + G[n][m]*Sigma[n][m] );
        //G0[n] = 1.0/ ( 1.0/G[n] + Sigma[n];
}

// Calculate the Hybridization function
void getDelta(std::complex<double> **delta,
              const std::complex<double> *const*G, const double *mu, const int &no_wn, const double &beta, const int &no_orb)
{
    for (int n=0; n<no_wn; n++)
        for (int m=0; m<no_orb; m++)
            delta[n][m] = wn(n,beta)*1.0*I + mu[m] - 1.0/G[n][m];
}

// Perform the Hilbert transform to get the imaginary frequency Green's function
void hilbert(std::complex<double> **G, const std::complex<double> *const*Sigma, const double &mu_lattice,
             const int &no_wn, const double *const*dos, const int &subHFDC, const double *hartreefock,
             const double &e_start, 
             const double &e_end, const int &e_steps,const double &beta, const int &no_orb)
{
    double de = (e_end-e_start)/e_steps;
    //check norm of dos
    for (int m=0; m<no_orb; m++)
    {
        double hilbert_norm = 0.0;
        for (int i=0; i<e_steps; i++)
            hilbert_norm += de * dos[i][m];

        for (int n=0; n<no_wn; n++)
        {
            G[n][m] = 0.0;

            for (int i=0; i<e_steps; i++)
            {
                double e = e_start + i*de;
                if (subHFDC==1)
                {
                    G[n][m] += (de/hilbert_norm) * dos[i][m]/( I*wn(n,beta) + mu_lattice - e - Sigma[n][m]+hartreefock[m] );
                }
                else
                {
                    G[n][m] += (de/hilbert_norm) * dos[i][m]/( I*wn(n,beta) + mu_lattice - e - Sigma[n][m] );
                }
            }

        }
    }
}

// Calculate the high-frequency coefficients of a Green's function
void getHfcoeff(std::vector<double> &coeffs, const std::complex<double> * const *Gf, const int &orb,
                const int &no_wn, const double &beta)
{
    int highFreqAvg = int(0.1*no_wn);
    int dwAvg = int(0.05*no_wn);

    double coeff0 = 0.0;
    double coeff1 = 0.0;
    double coeff2 = 0.0;
    double coeff3 = 0.0;

    for (int i=0; i<highFreqAvg; i++)
    {
        double wm = wn(no_wn-1-dwAvg-i,beta);
        double we = wn(no_wn-1-i,beta);
        double frm = Gf[no_wn-1-dwAvg-i][orb].real();
        double fre = Gf[no_wn-1-i][orb].real();
        double fim = Gf[no_wn-1-dwAvg-i][orb].imag();
        double fie = Gf[no_wn-1-i][orb].imag();
        double denom =  we*we - wm*wm;
        coeff0 += ( fre*we*we - frm*wm*wm )/denom;
        coeff1 += ( fim*wm*wm*wm - fie*we*we*we )/denom;
        coeff2 += wm*wm*we*we*( fre - frm )/denom;
        coeff3 += wm*wm*we*we*( fim*wm - fie*we )/denom;
    }
    coeff0 /= highFreqAvg;
    coeff1 /= highFreqAvg;
    coeff2 /= highFreqAvg;
    coeff3 /= highFreqAvg;

    coeffs.resize(4);
    coeffs[0] = coeff0;
    coeffs[1] = coeff1;
    coeffs[2] = coeff2;
    coeffs[3] = coeff3;
}

// Calculate the difference between two Green's functions
double Gdiff(const std::complex<double> *const*G1, const std::complex<double> *const*G2, const int &orb, const int &no_wn)
{
    double ret = 0.0;
    for (int n=0; n<no_wn; n++)
        if ( std::abs( G1[n][orb] - G2[n][orb] ) > ret ) 
            ret = std::abs( G1[n][orb] - G2[n][orb] );
    return ret;
}



// Calculate the local chemical potential for ctHyb
void getMu(double *mu, const std::complex<double> *const*G0, const int &no_wn, const double &beta, const int &no_orb)
{   //Real part of Hybrid function => Hybrid goes to zero for wn->infty
    
    std::complex<double> **G0inv = new std::complex<double>*[no_wn];
    for (int n=0; n<no_wn; n++)
    {
        G0inv[n] = new std::complex<double>[no_orb];
        for (int m=0; m<no_orb;m++)
            G0inv[n][m] = 1.0/G0[n][m];
    }
    std::vector<double> coeffs;
    for (int m=0; m<no_orb;m++)
    {
        getHfcoeff(coeffs, G0inv, m, no_wn, beta);
        mu[m] = coeffs[0];
    }
    for (int n=0; n<no_wn; n++)
        delete[] G0inv[n];
    delete[] G0inv;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Fourier transforms and fillings ///////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

// Calculate the filling of a Green's function
double getFilling(const std::complex<double> *const*G, const int &orb, const int &no_wn, const double &beta)
{
     //  We use high-freq. correction up to 4-th order
     std::vector<double> coeffs;
     getHfcoeff(coeffs, G, orb, no_wn, beta);

     double fill = 0.5 - coeffs[2]*beta/4.0 ; //+ coeffs[4]*beta*beta*beta/48.0 ;
     for (int n=0; n<no_wn; n++)
         fill += (2.0/beta)*( G[n][orb].real() + coeffs[2]/std::pow(wn(n,beta),2) );
                                          //- coeffs[4]/std::pow(wn(n,beta),4)  );
     return fill;
}

// Calculate the HartreeFock term
void getHartreeFock(double *hartreefock, const std::complex<double> *const*Gloc, const double &U, const double &J, 
                                    const int &no_wn, const int &no_orb, const double &beta)
{
    std::cout<<"Calculate static Hartree+Fock term..."<<std::endl;
    
    std::vector<double> init_fill(no_orb);
    for (int m=0; m<no_orb; m++)
    {
        init_fill[m] = getFilling(Gloc, m, no_wn, beta);
        std::cout<<"initital non-interacting filling orb="<<m<<" : "<<init_fill[m]<<std::endl;
    }
    
    for (int m1=0; m1<no_orb;m1++)
    {
        hartreefock[m1] = U*init_fill[m1]; // same orbital diff spin
        for (int m2=0; m2<no_orb;m2++)
        {
            if (m1!=m2)
            {
                hartreefock[m1] += (U-2*J)*init_fill[m2]; // other orbital diff spin
                hartreefock[m1] += (U-3*J)*init_fill[m2]; // other orbital same spin
            }
        }
    }
        
    for (int m1=0; m1<no_orb;m1++)
        std::cout<<"HartreeFock Orb="<<m1<<":  "<<hartreefock[m1]<<std::endl;
}

// Initialize the Selfenergy with the Hartree term
void initSigma(std::complex<double> **F, const int &no_wn, double *hartreefock, const int &no_orb)
{
    std::cout<<"Initialize the Selfenergy with the static Hartree+Fock term..."<<std::endl;
    for (int n=0; n<no_wn; n++)
    {
         for (int m1=0; m1<no_orb;m1++)
         {
            F[n][m1] = hartreefock[m1];
         }
    }
}

// determine the chemical potential for given filling
void determineMuLattice(double &mu_lattice, std::complex<double> **Gloc, const std::complex<double> *const*Sigma,
                        const int &no_wn, const double &init_fill_tot, const double &error,
                        const double *const*dos,  const int &subHFDC, const double *hartreefock, 
                        const double &e_start, const double &e_end, const int &e_steps,
                        const double &beta, const int &no_orb)
{
    // Perform regula falsi to find correct filling
    hilbert(Gloc, Sigma, mu_lattice, no_wn, dos, subHFDC, hartreefock, e_start, e_end, e_steps, beta, no_orb);

    double mu_a=mu_lattice;
    double mu_b=mu_lattice;
    double n_a = 0.0;
    for (int m=0; m<no_orb;m++)
        n_a += 2*getFilling(Gloc, m, no_wn, beta);   // *2 for spin
    double n_b = n_a;

    std::cout<<"Correcting total filling: desired="<<init_fill_tot<<", current="<<n_a<<std::endl;

    if ( n_a < init_fill_tot )
    {
        do
        {
            mu_a += 0.5;
            hilbert(Gloc, Sigma, mu_a, no_wn, dos, subHFDC, hartreefock, e_start, e_end, e_steps, beta, no_orb);
            n_a = 0.0;
            for (int m=0; m<no_orb;m++)
                n_a += 2*getFilling(Gloc, m, no_wn, beta);
            
            std::cout<<"mu = "<<mu_a<<" => filling = "<<n_a<<std::endl;
        } while ( n_a < init_fill_tot );
    }
    else
    {
        do
        {
            mu_b -= 0.5;
            hilbert(Gloc, Sigma, mu_b, no_wn, dos, subHFDC, hartreefock, e_start, e_end, e_steps, beta, no_orb);
            n_b = 0.0;
            for (int m=0; m<no_orb;m++)
                n_b += 2*getFilling(Gloc, m, no_wn, beta);
            
            std::cout<<"mu = "<<mu_b<<" => filling = "<<n_b<<std::endl;
        } while ( n_b > init_fill_tot );
    }

    //now we have mu_a with filling larger and mu_b with filling smaller than init_fill_tot
    while (1==1)
    {
        //std::cout<<"mu = ["<<mu_a<<", "<<mu_b<<"] => n = ["<<n_a<<", "<<n_b<<"]"<<std::endl;

        double mu_c = mu_b - (mu_b-mu_a)*(n_b - init_fill_tot)/( n_b - n_a );

        hilbert(Gloc, Sigma, mu_c, no_wn, dos, subHFDC, hartreefock, e_start, e_end, e_steps, beta, no_orb);
        double n_c = 0.0;
        for (int m=0; m<no_orb;m++)
            n_c +=  2*getFilling(Gloc, m, no_wn, beta);
        std::cout<<"mu lattice = "<<mu_c<<" => filling = "<<n_c<<std::endl;

        if (n_c > init_fill_tot)
        {
            n_a = n_c;
            mu_a = mu_c;
        }
        else
        {
            n_b = n_c;
            mu_b = mu_c;
        }

        if ( std::abs(n_a-init_fill_tot) < error )
        {
            mu_lattice = mu_a;
            break;
        }
        if ( std::abs(n_b-init_fill_tot) < error )
        {
            mu_lattice = mu_b;
            break;
        }
    }
    hilbert(Gloc, Sigma, mu_lattice, no_wn, dos, subHFDC, hartreefock, e_start, e_end, e_steps, beta, no_orb);
    double n_final = 0.0;
    for (int m=0; m<no_orb;m++)
        n_final +=  2*getFilling(Gloc, m, no_wn, beta);
    std::cout<<"Found new mu_lattice = "<<mu_lattice<<" with total filling="<<n_final<<std::endl;
}


// Fourier-transform the hybridization function onto imaginary time
void doFourier(const std::string &filename, const std::string &filename_back,
               const std::complex<double> *const*F, const int &no_wn, const int &no_t,
               const int &no_orb, const double &beta)
{   
    // Use 4-th order high-freq. correction for Fourier transform
    std::cout<<"Perform Fourier-transform on Hybridization function to imaginary time..."<<std::endl;
    std::fstream file_out(filename.c_str(), std::ios::out);

    std::vector< std::vector<double> >coeffs(no_orb);
    for (int m=0; m<no_orb;m++)
    {
        getHfcoeff(coeffs[m], F, m, no_wn, beta);
        for (int i=0; i<coeffs.size(); i++)
            std::cout<<"Hybrid hf-coeff for orb="<<m<<",  c"<<i<<": "<<coeffs[m][i]<<std::endl;
    }

    for (int it=0; it<=no_t; it++)
    {
        double dt = beta/no_t;
        double tau = it*dt;
        file_out<<tau<<'\t';
        
        for (int m=0; m<no_orb;m++)
        {
            double tmp = 0.0;
            for (int n=0; n<no_wn; n++)
                tmp += (2.0/beta)* ( F[n][m].real()*cos(wn(n,beta)*tau) + F[n][m].imag()*sin(wn(n,beta)*tau)
                                    +coeffs[m][1]*sin(wn(n,beta)*tau)/( wn(n,beta) )
                                    +coeffs[m][2]*cos(wn(n,beta)*tau)/std::pow(wn(n,beta), 2.0)
                                     -coeffs[m][3]*sin(wn(n,beta)*tau)/std::pow(wn(n,beta), 3.0) );

            // Now add the analytic part
            tmp += -coeffs[m][1]/2.0 + coeffs[m][2]*(2.0*tau-beta)/4.0
                +coeffs[m][3]*tau*(beta-tau)/4.0;

            // Ensure causality in case of numerical noise
            tmp = (tmp>0.0)? 0.0:tmp;

            file_out<<'\t'<<tmp<<'\t'<<tmp<<'\t';
        }
        file_out<<std::endl;
    }
    file_out.close();
}



/////////////////////////////////////////////////////////////////////////////////////////////
// Simple analytic continuation /////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

// Perform analytic continuation with Pade approximants
void pade(const std::string &filename, const std::complex<double> *const*G, const int &no_wn,
          const double &e_start, const double &e_end,const int &e_steps, const double &beta, const int &no_orb)
{
    int no_e = e_steps;
    double de = (e_end-e_start)*1.0/no_e;

    std::complex<double> ***pade_array = new std::complex<double>**[no_orb];
    for (int m=0; m<no_orb; m++)
    {
        pade_array[m] = new std::complex<double>*[no_wn];
        for (int i=0;  i<no_wn; i++)
        {
            pade_array[m][i] = new std::complex<double>[no_wn];
            pade_array[m][0][i] = G[i][m];
        }

        for (int k=1; k<no_wn; k++)
            for (int i=1; i<k+1; i++)
                pade_array[m][i][k] = (pade_array[m][i-1][i-1] - pade_array[m][i-1][k])
                                /( pade_array[m][i-1][k]*1.0*I*( wn(k,beta) - wn(i-1,beta) ) );
    }

    std::vector<double> integral(no_orb, 0.0);

    std::fstream file_out(filename.c_str(), std::ios::out);
    for (int e=0; e<no_e; e++)
    {
        double w = e_start + e*de;
        file_out<<w<<'\t';
        for (int m=0; m<no_orb; m++)
        {
            std::complex<double> temp = 1.0 + pade_array[m][no_wn-1][no_wn-1]*( w - I*wn(no_wn-2,beta) );
            for (int i=no_wn-2; i>=1; i--)
                temp = 1.0 + pade_array[m][i][i]*(w - I*wn(i-1,beta))/temp;
            temp = pade_array[m][0][0] / temp;

            file_out<<temp.real()<<'\t'<<temp.imag()<<'\t';
            integral[m] += -(temp.imag())*de;
        }
        file_out<<std::endl;
    }
    file_out.close();
    for (int m=0; m<no_orb; m++)
    {
        for (int i=0;  i<no_wn; i++)
            delete[] pade_array[m][i];
        delete[] pade_array[m];
    }
    delete[] pade_array;
}

//////////////////////////////////////////////////////////////////////////////////////////////
// In-and output related functions ///////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

// Read a Green's function from a file
void readMatsFunc(std::complex<double> **F, const std::string &filename, const int &no_wn, const int &no_orb)
{
    std::fstream readG(filename.c_str(), std::ios::in);
    std::string st;
    double reUp, reDown,imUp, imDown;
    for (int n=0; n<no_wn; n++)
    {
        readG >> st;
        for (int m=0; m<no_orb; m++)
        {
            readG >> reUp;
            readG >> imUp;
            F[n][m] = std::complex<double>( reUp , imUp );
        }
    }
    readG.close();
}

// Write a Green's function into a file
void writeG(const std::complex<double> *const*G, const std::string &filename, const int &no_wn,
                     const double &beta, const int &no_orb)
{
    std::fstream file_out(filename.c_str(), std::ios::out);
    for (int n=0; n<no_wn; n++)
    {
        file_out<<wn(n,beta)<<'\t';
        for (int m=0; m<no_orb; m++)
            file_out<<G[n][m].real()<<'\t'<<G[n][m].imag()<<'\t';
        file_out<<std::endl;
    }
    file_out.close();
}

// Write the interaction matrix into a file
void write_umatrix(const double &U, const double &J, const int &no_orb)
{
   std::fstream umatrix("imp/umatrix.dat", std::ios::out);
	if (no_orb==1) 
	{
	   umatrix<<0<<" "<<U<<std::endl;
	   umatrix<<U<<" "<<0<<std::endl;
	}
	else // Use kanamori Hamiltonian
	{
		for (int m1=0; m1<no_orb;m1++)
		{
			for (int s1=0; s1<2;s1++)
			{
				for (int m2=0; m2<no_orb;m2++)
				{
					for (int s2=0; s2<2;s2++)
					{
						if (m1==m2 && s1==s2)
							umatrix<< 0     <<" ";
						if (m1==m2 && s1!=s2)
							umatrix<< U     <<" ";
						if (m1!=m2 && s1==s2)
							umatrix<< U-3*J <<" ";
						if (m1!=m2 && s1!=s2)
							umatrix<< U-2*J <<" ";
					}
				}
				umatrix<<std::endl;
			}
		}
	}
   umatrix.close();
}

// Read the input paramaters from a file
void readInput(int &no_wn, int &ntau, double &U, double &J, double &t, 
               double &init_fill_tot, int &no_orb, double &beta, int &continueCalc,
                int &initHartreeFock, int &subHFDC, double &mixing, int &recalcDC, const std::string &filename)
{
    std::cout<<"Reading input file "<<filename<<"..."<<std::endl;
    std::string tmp;
    std::fstream inputfile(filename.c_str(), std::ios::in);

    inputfile >> no_orb; std::getline(inputfile,tmp);
    inputfile >> no_wn; std::getline(inputfile,tmp);
    inputfile >> ntau; std::getline(inputfile,tmp);
    inputfile >> U; std::getline(inputfile,tmp);
    inputfile >> J; std::getline(inputfile,tmp);
    inputfile >> t; std::getline(inputfile,tmp);
    inputfile >> init_fill_tot; std::getline(inputfile,tmp);
    inputfile >> beta; std::getline(inputfile,tmp);
    inputfile >> continueCalc; std::getline(inputfile,tmp);
    inputfile >> mixing; std::getline(inputfile,tmp);
    inputfile >> initHartreeFock; std::getline(inputfile,tmp);
    inputfile >> subHFDC; std::getline(inputfile,tmp);
    inputfile >> recalcDC; std::getline(inputfile,tmp);
    inputfile.close();

    std::cout<<"N_orbitals = "<<no_orb<<std::endl;
    std::cout<<"N_Matsubara = "<<no_wn<<std::endl;
    std::cout<<"N_Tau = "<<ntau<<std::endl;
    std::cout<<"U = "<<U<<std::endl;
    std::cout<<"J = "<<J<<std::endl;
    std::cout<<"t = "<<t<<std::endl;
    std::cout<<"total filling = "<<init_fill_tot<<std::endl;
    std::cout<<"beta = "<<beta<<std::endl;
    std::cout<<"ContinueCalc = "<<continueCalc<<std::endl;
    std::cout<<"mixing = "<<mixing<<std::endl;
    std::cout<<"initHartreeFock = "<<initHartreeFock<<std::endl;
    std::cout<<"subHartreeFockDC = "<<subHFDC<<std::endl<<std::endl;
    std::cout<<"recalcDC = "<<recalcDC<<std::endl<<std::endl;
}

// Read the Density of States used for the calculation
void readDosExtern(double **&dos, const std::string &filename, int &hilbert_esteps, 
                   double &hilbert_estart, double &hilbert_eend, const int &no_orb)
{
    std::cout<<"Reading DOS from file "<<filename<<"..."<<std::endl;

    hilbert_esteps = 0;
    std::string tmp;
    std::fstream inputfile(filename.c_str(), std::ios::in);
    while( std::getline(inputfile,tmp) )
        hilbert_esteps ++;
    inputfile.close();
    if (hilbert_esteps==0)
    {
        std::cout<<"ERROR: No data in "<<filename<<std::endl;
        exit(1);
    }
    std::cout<<"Found a DOS with "<<hilbert_esteps<<" data points..."<<std::endl;

    dos = new double*[hilbert_esteps];
    for (int i=0; i<hilbert_esteps; i++)
        dos[i] = new double[no_orb];
    

    inputfile.open(filename.c_str(), std::ios::in);
    double w;
    for (int i=0; i<hilbert_esteps; i++)
    {
        inputfile >> w;
        for (int m=0; m<no_orb; m++)
            inputfile >> dos[i][m];
        if (i==0)
            hilbert_estart = w;
        if (i==hilbert_esteps-1)
            hilbert_eend = w;
    }
    std::cout<<"E_min = "<<hilbert_estart<<std::endl;
    std::cout<<"E_max = "<<hilbert_eend<<std::endl<<std::endl;

    inputfile.close();
}

// Initialize the Density of States using a Bethe lattice
double fill_dos(double **dos, const int &ne, const double &estart, const double &eend, const double &t, const int &no_orb)
{
    //Bethe dos
    double de = (eend-estart)/ne;
    for (int i=0; i<ne; i++)
    {
        double e = estart + i*de;
        for (int m=0; m<no_orb; m++)
            dos[i][m] = ( std::abs(e)<=t )? std::sqrt(t*t-e*e)*2/(pi*t*t) : 0;
    }
}

// Check the high-frequency tail of a Green's function
void checkTail(const std::string &name, const std::complex<double> *const*G,  const int &no_wn, 
                          const double &beta, const int &no_orb)
{
    for (int m=0; m<no_orb; m++)
    {
        std::vector<double> coeffs;
        getHfcoeff(coeffs, G, m, no_wn, beta);
        std::cout<<"Hf-tail of "<<name<<",  orb="<<m<<" : "<<coeffs[1]<<std::endl;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////
// DMFT Impurity Solvers ////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

// Run the external CT-HYB solver
void externalSolver(std::complex<double> **Gimp, std::complex<double> **Sigma, 
                    const double &U, const double &J, const double *mu, 
                    const std::complex<double> *const*Delta, const int &no_wn, 
                    const int &ntau, const int &no_orb, const double &beta, const double &mixing)
{
    if (system("mkdir -p imp")!=0)
       std::cout<<"ERROR: Could not create folder imp!"<<std::endl;

    doFourier("imp/delta_0.dat", "imp/delta_0_back.dat", Delta, no_wn, ntau, no_orb, beta);
    write_umatrix(U, J, no_orb );

    std::fstream muvec("imp/mu_vector.dat", std::ios::out);
    for (int m=0; m<no_orb; m++)
	    muvec<<mu[m]<<"  "<<mu[m]<<"  ";
	 muvec<<std::endl;
    muvec.close();

    std::cout<<"All files written, start Impurity solver..."<<std::endl<<std::endl;
    if (system("cp alps_parm.dat imp/")!=0)
       std::cout<<"ERROR: Could not copy alps_parm.dat to imp folder!"<<std::endl;

    int retsolver = system("cd imp; mpirun -np 32 alps_cthyb alps_parm.dat > out.solver 2>&1 ");
    if (retsolver!=0)
    {
        std::cout<<"Error: Solver returned "<<retsolver<<std::endl;
        exit(1);
    }
    std::cout<<"Impurity solver finished successfully, read results..."<<std::endl;

    std::fstream readG("imp/Gwl.dat", std::ios::in);
    std::fstream readS("imp/Swl.dat", std::ios::in);
    std::string st;
    double re, im;
    for (int n=0; n<no_wn; n++)
    {
        readG >> st; //get rid of frequency
        readS >> st;
        //read and average Spins
        for (int m=0; m<no_orb; m++)
        {
            std::complex<double> tmpG = 0.0;
            std::complex<double> tmpS = 0.0;
            
            for (int i=0; i<2; i++)
            {
                readG >> re;
                readG >> im;
                tmpG += std::complex<double>( re , im );

                readS >> re;
                readS >> im;
                tmpS += std::complex<double>( re , im );
            }
            tmpG /= 2;
            tmpS /= 2;

            // mixing
            Gimp[n][m] = mixing*tmpG + (1-mixing)*Gimp[n][m];
            Sigma[n][m] = mixing*tmpS + (1-mixing)*Sigma[n][m];
        }
    }
    readG.close();
    readS.close();
}

// Hubbard I solver
/*
void hubbardI(std::complex<double> *G, const double &U, const double &eps,
              const double fill, const std::complex<double> *delta, const int &no_wn, const double &beta)
{   //Hubard I solver
    for (int n=0; n<no_wn; n++)
        G[n] = ( 1.0 + U*fill/( wn(n,beta)*1.0*I + eps - U - delta[n]) )
                /( wn(n,beta)*1.0*I + eps - delta[n] );
}
*/

//////////////////////////////////////////////////////////////////////////////////////////////
// Main program //////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    //////////////////////////////////////////
    // Define parameters and read input //////
    //////////////////////////////////////////
    //defaults, will be overwritten later
    int no_wn  = 1000;
    int ntau   = 1024;
    int no_orb = 1;
    double beta = 40.0;
    double U    = 2.0;
    double J    = U/4.0;
    double t    = 1.0;
    double init_fill_tot = 1.0;
    int hilbert_esteps=  5000;
    double hilbert_estart = -t-0.1;
    double hilbert_eend = t+0.1;
    int continueCalc = 0;
    int initHartreeFock = 1;
    int subHFDC = 0;
    double mixing = 1.0;
    int recalcDC = 0;

    double **dos;
    if (argc==2)
    {
        readInput(no_wn,ntau,U,J,t,init_fill_tot,no_orb,beta,continueCalc,initHartreeFock,subHFDC,mixing,recalcDC,argv[1]);
        hilbert_estart = -t-0.1;
        hilbert_eend = t+0.1;
        dos = new double*[hilbert_esteps];
        for (int i=0; i<hilbert_esteps; i++)
            dos[i] = new double[no_orb];
        fill_dos(dos, hilbert_esteps, hilbert_estart, hilbert_eend, t, no_orb);
    }
    else if (argc==3)
    {
        readInput(no_wn,ntau,U,J,t,init_fill_tot,no_orb,beta,continueCalc,initHartreeFock,subHFDC,mixing,recalcDC,argv[1]);
        readDosExtern(dos, argv[2], hilbert_esteps, hilbert_estart, hilbert_eend, no_orb);
    }
    else
    {
        std::cout<<"ERROR: Wrong number of input parameters!"<<std::endl;
		  std::cout<<"Provide the input file OR input file and DOS!"<<std::endl;
        exit(1);
    }

    double mu_lattice = 0.0;
    int pade_esteps  =  2000;
    double convLimit = 1.0e-4;
    double nerror = 0.0001;
    double pade_estart = -10.0;
    double pade_eend = 10.0;

    double *mu = new double[no_orb];
    double *hartreefock = new double[no_orb];
    std::complex<double> **G0 = new std::complex<double>*[no_wn];
    std::complex<double> **Gimp = new std::complex<double>*[no_wn];
    std::complex<double> **Gloc = new std::complex<double>*[no_wn];
    std::complex<double> **Sigma = new std::complex<double>*[no_wn];
    std::complex<double> **Delta = new std::complex<double>*[no_wn];
    for (int n=0; n<no_wn; n++)
    {
        G0[n] = new std::complex<double>[no_orb];
        Gimp[n] = new std::complex<double>[no_orb];
        Gloc[n] = new std::complex<double>[no_orb];
        Sigma[n] = new std::complex<double>[no_orb];
        Delta[n] = new std::complex<double>[no_orb];
    }
    std::cout<<"Use total filling = "<<init_fill_tot<<std::endl;
    
    // Calculate HartreeFock term
    std::cout<<"Calculate nonint filling for hartreefock"<<std::endl;
    for (int n=0; n<no_wn; n++)
        for (int m=0; m<no_orb; m++)
        {
            hartreefock[m] = 0.0;
            Sigma[n][m] = 0.0;
        }        
    determineMuLattice(mu_lattice, Gloc, Sigma, no_wn, init_fill_tot, nerror, dos, subHFDC, hartreefock,hilbert_estart, 
                                        hilbert_eend, hilbert_esteps,beta, no_orb);
    hilbert(Gloc, Sigma, mu_lattice, no_wn, dos, subHFDC, hartreefock, hilbert_estart, hilbert_eend, hilbert_esteps,beta, no_orb);
    getHartreeFock(hartreefock,Gloc,U,J,no_wn,no_orb,beta);
    ///////////////////////////////////

    std::cout<<"All input parameters have been read successfully, initialize DMFT calculation..."<<std::endl;
    if (continueCalc==1)
       readMatsFunc(Sigma, "Sigma.dat", no_wn, no_orb);
    else
    {
        if (initHartreeFock==1)
            initSigma(Sigma, no_wn, hartreefock, no_orb);
    }    

    //////////////////////////////////////////
    // Now we have proper input! /////////////
    // Start preparing the DMFT cycle ////////
    //////////////////////////////////////////
    std::cout<<"Calculate Gloc and the filling..."<<std::endl;
    determineMuLattice(mu_lattice, Gloc, Sigma, no_wn, init_fill_tot, nerror, dos, subHFDC, hartreefock,hilbert_estart, 
                       hilbert_eend, hilbert_esteps,beta, no_orb);
    hilbert(Gloc, Sigma, mu_lattice, no_wn, dos, subHFDC, hartreefock, hilbert_estart, hilbert_eend, hilbert_esteps,beta, no_orb);
    std::cout<<"Obtain G0 from Dyson's equation..."<<std::endl;
    getG0Dyson(G0, Sigma, Gloc, no_wn, no_orb);
    std::cout<<"Filling G0:  ";
    for (int m=0; m<no_orb;m++)
        std::cout<<getFilling(G0, m, no_wn, beta)<<"   ";
    std::cout<<std::endl;

    // Calculate the local chemical potential
    getMu(mu, G0, no_wn, beta, no_orb);
    std::cout<<"Determine the initial local impurity potential mu_loc from G0 as ";
    for (int m=0; m<no_orb;m++)
        std::cout<<mu[m]<<"   ";
    std::cout<<std::endl;

    // Calculate the hybridization function
    std::cout<<"Get the initial Hybridization function..."<<std::endl;
    getDelta(Delta, G0, mu, no_wn, beta,no_orb);

	 // init the impurity GF with Gloc
    for (int n=0; n<no_wn; n++)
        for (int m=0; m<no_orb; m++)
            Gimp[n][m] = Gloc[n][m];

    // write inital data    
    checkTail("Gloc",G0,no_wn,beta, no_orb);
    checkTail("G0",Gloc,no_wn,beta, no_orb);
    pade("Gloc_init_cont.dat", Gloc, no_wn, pade_estart, pade_eend, pade_esteps,beta,no_orb);
    pade("Gbath_init_cont.dat", G0, no_wn, pade_estart, pade_eend, pade_esteps,beta,no_orb);
    pade("Sigma_init_cont.dat", Sigma, no_wn, pade_estart, pade_eend, pade_esteps,beta,no_orb);
    pade("Hybrid_init_cont.dat", Delta, no_wn, pade_estart, pade_eend, pade_esteps,beta,no_orb);
    writeG(G0, "Gbath_init.dat", no_wn,beta,no_orb);
    writeG(Gloc, "Gloc_init.dat", no_wn,beta,no_orb);
    writeG(Sigma, "Sigma_init.dat", no_wn,beta,no_orb);
    writeG(Delta, "Hybrid_init.dat", no_wn,beta,no_orb);
    std::cout<<std::endl;

    //////////////////////////////////////////
    // Start the DMFT cycle //////////////////
    //////////////////////////////////////////
    std::cout<<"Start the DMFT self-consistency cycle..."<<std::endl;
    for (int i=0; i<30; i++)
    {
        std::string itstring = std::to_string(i);
        std::cout<<"Iteration "<<itstring<<":"<<std::endl;

        // Solve the impurity model and obtain the Selfenergy
        //hubbardI(Gimp, U, U/2, fillG0, Delta, no_wn, beta);
        externalSolver(Gimp, Sigma, U, J, mu, Delta, no_wn, ntau, no_orb, beta, mixing);

        //getSigma(Sigma, G0, Gimp, no_wn);
        checkTail("Gimp",Gimp,no_wn,beta, no_orb);
        checkTail("Simp",Sigma,no_wn,beta, no_orb);
        if ( recalcDC==1 )
	        getHartreeFock(hartreefock,Gimp,U,J,no_wn,no_orb,beta);

        // Readjust chemical potential for correct filling
        std::cout<<"Calculate Gloc and the filling..."<<std::endl;
        determineMuLattice(mu_lattice, Gloc, Sigma, no_wn, init_fill_tot, nerror, 
                           dos,subHFDC, hartreefock, hilbert_estart, hilbert_eend, hilbert_esteps, beta, no_orb);

        // Calculate new local Green's function
        hilbert(Gloc, Sigma, mu_lattice, no_wn, dos, subHFDC, hartreefock,hilbert_estart, hilbert_eend, hilbert_esteps,beta, no_orb);
        checkTail("Gloc",Gloc,no_wn,beta, no_orb);

        // Get new bath Green's function
        std::cout<<"Get new bath Green's function G0..."<<std::endl;
        getG0Dyson(G0, Sigma, Gloc, no_wn, no_orb);
        std::cout<<"Filling G0:  ";
        for (int m=0; m<no_orb;m++)
            std::cout<<getFilling(G0, m, no_wn, beta)<<"   ";
        std::cout<<std::endl;

        // Calculate the local chemical potential
        getMu(mu, G0, no_wn, beta, no_orb);
        std::cout<<"Determine the local impurity potential mu_loc from G0 as ";
        for (int m=0; m<no_orb;m++)
            std::cout<<mu[m]<<"   ";
        std::cout<<std::endl;

        // Calculate the hybridization function
        std::cout<<"Get the Hybridization function..."<<std::endl;
        getDelta(Delta, G0, mu, no_wn, beta,no_orb);

    //////////////////////////////////////////
    // Write information and data ////////////
    //////////////////////////////////////////
        std::cout<<"Filling G0: ";
        for (int m=0; m<no_orb;m++)
            std::cout<< getFilling(G0, m, no_wn, beta)<<"   ";
        std::cout<<std::endl;
        std::cout<<"Filling Gloc: ";
        for (int m=0; m<no_orb;m++)
            std::cout<< getFilling(Gloc, m, no_wn, beta)<<"   ";
        std::cout<<std::endl;
        std::cout<<"Filling Gimp: ";
        for (int m=0; m<no_orb;m++)
            std::cout<< getFilling(Gimp, m, no_wn, beta)<<"   ";
        std::cout<<std::endl;
        std::cout<<"|Gimp-Gloc| :  ";
        for (int m=0; m<no_orb;m++)
            std::cout<<Gdiff(Gimp, Gloc, m, no_wn)<<"   ";
        std::cout<<std::endl;
        std::cout<<"Iteration "<<i<<" complete."<<std::endl;
        std::cout<<std::endl;

        writeG(G0, "Gbath.dat_"+itstring, no_wn,beta,no_orb);
        writeG(Gimp, "Gimp.dat_"+itstring, no_wn,beta,no_orb);
        writeG(Gloc, "Gloc.dat_"+itstring, no_wn,beta,no_orb);
        writeG(Sigma, "Sigma.dat_"+itstring, no_wn,beta,no_orb);
        writeG(Delta, "Hybrid.dat_"+itstring, no_wn,beta,no_orb);

        pade("Gbath_cont.dat_"+itstring, G0, no_wn, pade_estart, pade_eend, pade_esteps,beta,no_orb);
        pade("Gimp_cont.dat_"+itstring, Gimp, no_wn, pade_estart, pade_eend, pade_esteps,beta,no_orb);
        pade("Gloc_cont.dat_"+itstring, Gloc, no_wn, pade_estart, pade_eend, pade_esteps,beta,no_orb);
        pade("Hybrid_cont.dat_"+itstring, Delta, no_wn, pade_estart, pade_eend, pade_esteps,beta,no_orb);
        pade("Sigma_cont.dat_"+itstring, Sigma, no_wn, pade_estart, pade_eend, pade_esteps,beta,no_orb);
        std::cout<<std::endl;

        // Check for convergence
        double totdiff = 0.0;
        for (int m=0; m<no_orb;m++)
            totdiff += Gdiff(Gimp, Gloc, m, no_wn)/no_orb;
        if (totdiff < convLimit)
        {
            std::cout<<"Converged!"<<std::endl<<std::endl;
            break;
        }
    }

    // Clean up
    for (int n=0; n<no_wn; n++)
    {
        delete[] G0[n];
        delete[] Gimp[n];
        delete[] Gloc[n];
        delete[] Sigma[n];
        delete[] Delta[n];
    }
    for (int i=0; i<hilbert_esteps; i++)
        delete[] dos[i];
    delete[] mu;
    delete[] hartreefock;
    return 0;
}

