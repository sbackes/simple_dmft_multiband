CXX=icc -std=c++11
LDFLAGS=    #-lgsl -lgslcblas
CXXFLAGS=	#-g -Wall -Wextra
DEFINES=
OFLAGS=-O3

SRCDIR=./
OBJDIR=./
EXECUTABLE=dmft

#VPATH = $(SRCDIR)

# generate lists of object files (all and existing only)
OBJECTS_ALL = $(patsubst %.cpp,$(OBJDIR)%.o,$(wildcard *.cpp))
OBJECTS_EXT = $(wildcard $(OBJDIR)*.o)

# define implicit rule to compile a cpp file
$(OBJDIR)%.o : %.cpp
	$(CXX) $(CXXFLAGS) $(OFLAGS) $(DEFINES) -c $< -o $@
	$(CXX) -MM -MT $(OBJDIR)$*.o $(CXXFLAGS) $(OFLAGS) $(DEFINES) $< > $(OBJDIR)$*.d

# executable
$(OBJDIR)$(EXECUTABLE) : $(OBJECTS_ALL)
	$(CXX) $(CXXFLAGS) $(OFLAGS) $^ $(LDFLAGS) -o $@

# pull in dependency info for existing object files
-include $(OBJECTS_EXT:.o=.d)

clean :
	rm -f dmft *.o *.d
